from constants import UI_PADDING
from simulation import Simulation
from ui import UI
import pygame as pg
import pygame_gui as gui


class Config:
    def __init__(self, screen: pg.Surface, manager, sim: Simulation) -> None:

        self.sim = sim
        self.screen = screen
        self.manager = manager
        self.window = gui.elements.UIWindow(
            self.screen.get_rect(), self.manager, "Configure Collision"
        )
        # self.window.set_dimensions()
        self.window.hide()

        self.generate_content()

        button_rect = pg.Rect((0, 0), (100, 40))
        self.toggle = False
        self.toggle_button = gui.elements.UIButton(
            button_rect,
            "Config",
            self.manager,
            tool_tip_text="Toggle the config window",
        )

    def generate_content(self):
        seed_label_rect = pg.Rect((0, 0), (100, 30))
        seed_label_rect.topleft = (UI_PADDING, UI_PADDING)
        seed_layout_rect = pg.Rect((0, 0), (100, 30))
        seed_layout_rect.topleft = seed_label_rect.topright
        self.seed_label = gui.elements.UILabel(
            relative_rect=seed_label_rect,
            manager=self.manager,
            text="Random Seed",
            container=self.window,
        )

        self.seed_input = gui.elements.UITextEntryLine(
            relative_rect=seed_layout_rect, manager=self.manager, container=self.window
        )

        area = seed_label_rect.unionall((seed_label_rect, seed_layout_rect))
        self.window.set_dimensions(
            (area.width + UI_PADDING * 2, area.height + UI_PADDING * 2)
        )

    def update(self, dt):
        pass

    def process_events(self, event):
        self.window.process_event(event)

        if event.type == pg.USEREVENT:
            if event.user_type == gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.toggle_button:
                    if self.toggle:
                        self.window.hide()
                        self.toggle = False
                    else:
                        self.window.show()
                        self.toggle = True
