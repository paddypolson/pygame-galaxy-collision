import pygame as pg


class FPSModule:
    def __init__(self):

        self.clock = pg.time.Clock()
        self.font = pg.font.Font(None, 30)
        self.colour = "white"
        self.display = True

    def tick(self, fps: int):
        return self.clock.tick(fps)

    def draw(self, surface: pg.Surface):
        if self.display:
            fps_text = str(round(self.clock.get_fps(), 1)) + " FPS"
            text = self.font.render(fps_text, True, self.colour)
            surface.blit(text, (0, 0))
