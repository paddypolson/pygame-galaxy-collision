from random import randint, random, seed, uniform
from pygame import Vector2
from body import Body
from galaxy import Galaxy


class Simulation:
    def __init__(self, rect):

        self.rect = rect
        self.bodies_per_galaxy = 2000
        self.seed = 0

        self.generate()

    def generate(self):

        seed(self.seed)

        self.bodies = []
        self.centers = []

        rotation = randint(0, 360)
        offset = 3
        mass_offset = 5000

        self.centers.append(
            Galaxy(
                20000.0 + randint(-mass_offset, mass_offset), 1, "red", randint(0, 1)
            )
        )
        self.centers.append(
            Galaxy(
                20000.0 + randint(-mass_offset, mass_offset), 1, "red", randint(0, 1)
            )
        )

        pos_vec = Vector2(250, 20).rotate(rotation)
        vel_vec = Vector2(-60, 0).rotate(rotation)

        self.centers[0].pos = (
            pos_vec
            + self.rect.center
            + (uniform(-offset, offset), uniform(-offset, offset))
        )
        self.centers[0].vel = vel_vec + (
            uniform(-offset, offset),
            uniform(-offset, offset),
        )

        self.centers[1].pos = -pos_vec + self.rect.center
        self.centers[1].vel = -vel_vec

        for _ in range(self.bodies_per_galaxy):
            self.bodies.append(Body.create_body(self.centers[0], "yellow"))
            self.bodies.append(Body.create_body(self.centers[1], "orange"))

    def set_seed(self, seed: str):
        """Parse a string for a seed, random seed if nothing is provided"""
        if not seed:
            self.seed = random()
        else:
            self.seed = seed

    def update(self, dt):
        self.centers[0].pulled_by(self.centers[1])
        self.centers[1].pulled_by(self.centers[0])

        for body in self.bodies:
            body.pulled_by(self.centers[0])
            body.pulled_by(self.centers[1])

        self.centers[0].update(dt)
        self.centers[1].update(dt)

        self.bodies = [body for body in self.bodies if not body.destroy]

        for body in self.bodies:
            body.update(dt)

    def draw(self, surface):
        for body in self.bodies:
            body.draw(surface)
        for center in self.centers:
            center.draw(surface)
