from body import Body
from pygame import Vector2, gfxdraw

import pygame as pg


class Galaxy(Body):
    def __init__(self, mass: float, radius: float, colour, clockwise=True):
        super().__init__(mass, radius, colour=colour)

        self.show_velocity = False
        self.clockwise = clockwise

    def draw_velocity(self, surface):
        """
        Render a velocity vector, rotate and blit to surface
        Currently not working
        """
        scaler = 5
        width = 10
        arrow = pg.Surface((self.vel.length() * scaler, width))
        tri_points = [(0, width / 2), (6, width), (6, 0)]
        end = self.pos + (self.vel * scaler)
        gfxdraw.hline(arrow, 0, arrow.get_width(), int(width / 2), (255, 0, 0))
        gfxdraw.filled_polygon(arrow, tri_points, (255, 0, 0))
        angle = self.vel.angle_to(Vector2(-1, 0))
        arrow = pg.transform.rotate(arrow, angle)
        surface.blit(arrow, self.pos)

    def draw(self, surface):
        if self.show_velocity:
            self.draw_velocity(surface)
        return super().draw(surface)
