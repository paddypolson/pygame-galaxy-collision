from sys import exit
from ui_configure import Config

from simulation import Simulation
from ui import UI
from constants import *

import pygame as pg
import pygame_gui as gui

#####
pg.init()
pg.display.set_caption("Galaxy Collision Simulator")
width, height = 1280, 720
screen = pg.display.set_mode((width, height), pg.SCALED)
rect = pg.Rect(0, 0, width, height)


def main():

    sim = Simulation(rect)
    ui = UI(screen, sim)
    config = Config(screen, ui.manager, sim)
    ui.config = config
    # ui.manager.set_visual_debug_mode(True)

    while True:

        real_dt = ui.tick(60)

        # Plank time!
        sim_dt = 1 / 120  # Calculation is much more stable if given a constaint dt
        screen.fill("black")

        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                exit()

            if (event.type == pg.KEYUP) and (event.key == pg.K_ESCAPE):
                pg.quit()
                exit()

            ui.process_events(event)

        sim.update(sim_dt)
        ui.update(real_dt)

        sim.draw(screen)
        ui.draw(screen)

        pg.display.flip()


if __name__ == "__main__":
    main()
