from simulation import Simulation
from fps_module import FPSModule
from constants import *

import pygame as pg
import pygame_gui as gui


class UI:
    def __init__(self, screen: pg.Surface, sim: Simulation):

        self.screen = screen
        self.rect = screen.get_rect()
        self.sim = sim
        self.control_buttons = []

        self.config = None

        self.fps_module = FPSModule()
        self.manager = gui.UIManager((self.rect.width, self.rect.height))
        self.total_number_of_bodies = str(0)

        reset_layout_rect = pg.Rect((0, 0), (100, 40))
        reset_layout_rect.bottomleft = (UI_PADDING, -UI_PADDING)

        bodies_label_rect = pg.Rect((0, 0), (128, 30))
        bodies_label_rect.bottomleft = (UI_PADDING, reset_layout_rect.top - UI_PADDING)
        bodies_layout_rect = pg.Rect((0, 0), (100, 30))
        bodies_layout_rect.topleft = bodies_label_rect.topright

        self.reset_button = gui.elements.UIButton(
            relative_rect=reset_layout_rect,
            text="Reset",
            manager=self.manager,
            anchors={
                "left": "left",
                "right": "left",
                "top": "bottom",
                "bottom": "bottom",
            },
        )

        self.bodies_label = gui.elements.UILabel(
            relative_rect=bodies_label_rect,
            manager=self.manager,
            text="Number of Bodies",
            anchors={
                "left": "left",
                "right": "left",
                "top": "bottom",
                "bottom": "bottom",
            },
        )

        self.bodies_output = gui.elements.UILabel(
            relative_rect=bodies_layout_rect,
            manager=self.manager,
            text=self.total_number_of_bodies,
            anchors={
                "left": "left",
                "right": "left",
                "top": "bottom",
                "bottom": "bottom",
            },
        )

    def tick(self, fps):
        return self.fps_module.tick(fps)

    def update(self, dt):
        self.bodies_output.set_text(str(len(self.sim.bodies)))
        self.config.update(dt)
        self.manager.update(dt)

    def draw(self, surface):
        self.fps_module.draw(surface)
        self.manager.draw_ui(surface)

    def process_events(self, event):

        if event.type == pg.USEREVENT:
            if event.user_type == gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.reset_button:
                    self.sim.set_seed(self.config.seed_input.get_text())
                    self.sim.generate()

        self.config.process_events(event)
        self.manager.process_events(event)
