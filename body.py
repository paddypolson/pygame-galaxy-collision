from __future__ import annotations
from pygame import Vector2, sprite
from math import sqrt, sin, cos, pi
from random import uniform
from constants import *

import pygame as pg


class Body(sprite.Sprite):
    def __init__(self, mass: float, radius: float, colour="yellow"):
        sprite.Sprite.__init__(self)

        self.mass = mass
        self.radius = radius
        self.pos = Vector2()
        self.vel = Vector2()
        self.acc = Vector2()
        self.colour = colour
        self.destroy = False
        self.softening_factor = 0.2

        self.image = pg.Surface([1, 1])
        self.image.fill(self.colour)
        self.rect = self.image.get_rect()

    def pulled_by(self, other_body: Body):
        dist = self.pos.distance_to(other_body.pos) + self.softening_factor
        if dist < other_body.radius:
            self.destroy = True
        self.acc += (
            G * other_body.mass * (other_body.pos - self.pos) / dist / dist / dist
        )

    def update(self, dt: float):
        self.vel += dt * self.acc
        self.pos += dt * self.vel
        self.acc = Vector2()

    def draw(self, surface: pg.Surface):
        surface.blit(self.image, self.pos)

    @classmethod
    def create_body(cls, center: Body, colour):

        mass = uniform(0.5, 1.5)

        body = Body(1, mass, colour)
        theta = uniform(0, 2 * pi)
        r = uniform(15, MAX_RADIUS)

        r = r * r / MAX_RADIUS  # change distribution of particles
        body.pos = Vector2(r * cos(theta), r * sin(theta))
        body.pos += center.pos

        v = sqrt(G * center.mass / r)
        if center.clockwise:
            body.vel = Vector2(-v * sin(theta), v * cos(theta))
        else:
            body.vel = Vector2(v * sin(theta), -v * cos(theta))
        offset = 0.5
        body.vel += Vector2(uniform(-offset, offset), uniform(-offset, offset))
        body.vel += center.vel

        return body
