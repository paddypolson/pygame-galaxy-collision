# Pygame Galaxy Collision Simulator
This program will simulate two galaxies colliding using Newtons Law of Universal Gravitation. The simulation parameters are randomly generated from a seed and uses the Euler method to step the simulation forward. Currently the number of bodies per galaxy is set at 2000, for a total starting number of 4000. Each body is influenced only by the galacitic center of mass (red dot) from both galaxies, effectivly creating 4000 individual 3-body simulations. This is much easier computationally than a full 4000-body simulation yet still produces satisfying results.

This is not really how galaxies are really governed, General Relitivity would be a better fit (and also adding dark matter) but they are a little bit beyond the scope of the project. 

![Alt Text](https://thumbs.gfycat.com/ImpoliteForcefulHornedviper-size_restricted.gif)

### Requirements 
- pygame 2
- pygame_gui

### To Run
~~~bash
pip install -r requirements.txt
python main.py
~~~